<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
	<title>仿京东触屏版html5手机wap购物网站模板下载</title>
	<meta name="author" content="m.jd.com">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/shop/css/base2013.css" charset="gbk" />
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/shop/css/2013/sale/sale.css" charset="gbk" />
	<link rel="apple-touch-icon-precomposed" href="<%=basePath%>/static/shop/m.jd.com/images/apple-touch-icon.png" />
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/html5/common.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/html5/spin.min.js"></script>
</head>