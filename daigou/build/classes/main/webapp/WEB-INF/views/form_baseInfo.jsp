<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path;
%>
<!-- begin row -->
<div class="row">
	 <!-- col-md-12 -->
        <div class="col-md-12">
        	<!-- begin  tabbable-line-->
            <div class="tabbable-line boxless tabbable-reversed">
          	 	<ul class="nav nav-tabs">
                  <li class="active">
                      <a href="#tab_0" data-toggle="tab">基本资料</a>
                  </li>
                  <li>
                      <a href="#tab_1" data-toggle="tab">工作经历</a>
                  </li>
                  <li>
                      <a href="#tab_2" data-toggle="tab">项目经验</a>
                  </li>
                  <li>
                      <a href="#tab_3" data-toggle="tab">其他</a>
                  </li>
              	</ul>
                <div class="tab-content">
                		<!-- begin tab_0 -->
                    <div class="tab-pane active" id="tab_0">
                        <div class="portlet box green">
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="portlet light bordered" id="form_wizard_1">
           <div class="portlet-title">
               <div class="caption">
                   <i class=" icon-layers font-red"></i>
                   <span class="caption-subject font-red bold uppercase">完善基本信息
                       <span class="step-title">4个步骤,即可完成</span>
                   </span>
               </div>
               <div class="actions">
                   <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                       <i class="icon-cloud-upload"></i>
                   </a>
                   <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                       <i class="icon-wrench"></i>
                   </a>
                   <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                       <i class="icon-trash"></i>
                   </a>
               </div>
           </div>
           <div class="portlet-body form">
               <form action="#" class="form-horizontal" id="submit_form" method="POST">
                   <div class="form-wizard">
                       <div class="form-body">
                           <ul class="nav nav-pills nav-justified steps">
                               <li>
                                   <a href="#tab1" data-toggle="tab" class="step">
                                       <span class="number"> 1 </span>
                                       <span class="desc">
                                           <i class="fa fa-check"></i>个人基本信息</span>
                                   </a>
                               </li>
                               <li>
                                   <a href="#tab2" data-toggle="tab" class="step">
                                       <span class="number"> 2 </span>
                                       <span class="desc">
                                           <i class="fa fa-check"></i>求职意向</span>
                                   </a>
                               </li>
                               <li>
                                   <a href="#tab3" data-toggle="tab" class="step active">
                                       <span class="number"> 3 </span>
                                       <span class="desc">
                                           <i class="fa fa-check"></i>专业技能</span>
                                   </a>
                               </li>
                               <li>
                                   <a href="#tab4" data-toggle="tab" class="step">
                                       <span class="number"> 4 </span>
                                       <span class="desc">
                                           <i class="fa fa-check"></i>发布分享</span>
                                   </a>
                               </li>
                           </ul>
                           <div id="bar" class="progress progress-striped" role="progressbar">
                               <div class="progress-bar progress-bar-success"> </div>
                           </div>
                           <div class="tab-content">
                               <div class="alert alert-danger display-none">
                                   <button class="close" data-dismiss="alert"></button>请检查以下必填项</div>
                               <div class="alert alert-success display-none">
                                   <button class="close" data-dismiss="alert"></button> Your form validation is successful! </div>
                               <div class="tab-pane active" id="tab1">
                                   <h3 class="block">请完善您的基本信息</h3>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">姓名
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="username" />
                                           <span class="help-block">输入您的真实姓名</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">性别
                                       </label>
                                       <div class="col-md-4">
                                           <div class="radio-list">
                                                   <input type="radio" name="gender" value="M" data-title="Male" />男</label>
                                                   <input type="radio" name="gender" value="F" data-title="Female" />女</label>
                                                   <input type="radio" name="gender" value="MF" data-title="F" />保密</label>
                                           </div>
                                           <div id="form_gender_error"> </div>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">出生年月
                                       </label>
                                       <div class="col-md-4">
                                           <input type="password" class="form-control" name="password" id="submit_form_password" />
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">籍贯
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的户籍所在地</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">毕业院校
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的毕业院校(最高学历)</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">毕业时间:
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的毕业时间(最高学历)</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                          <label class="control-label col-md-3">学历</label>
                                          <div class="col-md-4">
                                              <select name="country" id="country_list" class="form-control">
                                                  <option value=""></option>
                                                  <option value="G">高中</option>
                                                  <option value="Z">专科</option>
                                                  <option value="B">本科</option>
                                                  <option value="SS">硕士</option>
                                                  <option value="BS">博士</option>
                                                  <option value="Q">其他</option>
                                              </select>
                                          </div>
                                      </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">专业:
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的所学专业</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">工作年限:
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的工作年限(从毕业开始)</span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">政治面貌:
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="email" />
                                           <span class="help-block">请输入您的政治面貌</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="tab-pane" id="tab2">
                                   <h3 class="block">请完善您的受教育培训经历</h3>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Fullname
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="fullname" />
                                           <span class="help-block"> Provide your fullname </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Phone Number
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="phone" />
                                           <span class="help-block"> Provide your phone number </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Gender
                                       </label>
                                       <div class="col-md-4">
                                           <div class="radio-list">
                                               <label>
                                                   <input type="radio" name="gender" value="M" data-title="Male" /> Male </label>
                                               <label>
                                                   <input type="radio" name="gender" value="F" data-title="Female" /> Female </label>
                                           </div>
                                           <div id="form_gender_error"> </div>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Address
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="address" />
                                           <span class="help-block"> Provide your street address </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">City/Town
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="city" />
                                           <span class="help-block"> Provide your city or town </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Country</label>
                                       <div class="col-md-4">
                                           <select name="country" id="country_list" class="form-control">
                                               <option value=""></option>
                                               <option value="AF">Afghanistan</option>
                                               <option value="AL">Albania</option>
                                           </select>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Remarks</label>
                                       <div class="col-md-4">
                                           <textarea class="form-control" rows="3" name="remarks"></textarea>
                                       </div>
                                   </div>
                               </div>
                               <div class="tab-pane" id="tab3">
                                   <h3 class="block">请完善您的工作经历</h3>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Card Holder Name
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="card_name" />
                                           <span class="help-block"> </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Card Number
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" class="form-control" name="card_number" />
                                           <span class="help-block"> </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">CVC
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" placeholder="" class="form-control" name="card_cvc" />
                                           <span class="help-block"> </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Expiration(MM/YYYY)
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <input type="text" placeholder="MM/YYYY" maxlength="7" class="form-control" name="card_expiry_date" />
                                           <span class="help-block"> e.g 11/2020 </span>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Payment Options
                                           <span class="required"> * </span>
                                       </label>
                                       <div class="col-md-4">
                                           <div class="checkbox-list">
                                               <label>
                                                   <input type="checkbox" name="payment[]" value="1" data-title="Auto-Pay with this Credit Card." /> Auto-Pay with this Credit Card </label>
                                               <label>
                                                   <input type="checkbox" name="payment[]" value="2" data-title="Email me monthly billing." /> Email me monthly billing </label>
                                           </div>
                                           <div id="form_payment_error"> </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="tab-pane" id="tab4">
                                   <h3 class="block">请完善您的项目经验</h3>
                                   <h4 class="form-section">Account</h4>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Username:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="username"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Email:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="email"> </p>
                                       </div>
                                   </div>
                                   <h4 class="form-section">Profile</h4>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Fullname:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="fullname"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Gender:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="gender"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Phone:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="phone"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Address:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="address"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">City/Town:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="city"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Country:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="country"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Remarks:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="remarks"> </p>
                                       </div>
                                   </div>
                                   <h4 class="form-section">Billing</h4>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Card Holder Name:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="card_name"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Card Number:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="card_number"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">CVC:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="card_cvc"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Expiration:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="card_expiry_date"> </p>
                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label class="control-label col-md-3">Payment Options:</label>
                                       <div class="col-md-4">
                                           <p class="form-control-static" data-display="payment[]"> </p>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="form-actions">
                           <div class="row">
                               <div class="col-md-offset-3 col-md-9">
                                   <a href="javascript:;" class="btn default button-previous">
                                       <i class="fa fa-angle-left"></i> Back </a>
                                   <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                       <i class="fa fa-angle-right"></i>
                                   </a>
                                   <a href="javascript:;" class="btn green button-submit"> Submit
                                       <i class="fa fa-check"></i>
                                   </a>
                               </div>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
       </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                   </div>
                   <!-- end tab_0 -->
                   <!-- begin tab_1 -->
                   <div class="tab-pane" id="tab_1">
                         <div class="portlet box blue">
                             <div class="portlet-body form">
                             <!-- BEGIN FORM-->
                                <table class="table table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>起始年月</th>
			<th>结束年月</th>
			<th class="hidden-480">教育机构</th>
			<th>获得证书</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td>2007年9月</td>
			<td>2011年7月</td>
			<td class="hidden-480">北京大学</td>
			<td>毕业证、学位证</td>
		</tr>
		<tr>
			<td>2</td>
			<td>2004</td>
			<td>2007</td>
			<td class="hidden-480">北京四中</td>
			<td>毕业证</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td class="hidden-480"></td>
			<td></td>
		</tr>
	</tbody>
</table>
                               <!-- END FORM-->
                   	      </div>
                   	  </div>
                   </div>
                   <!-- end tab_1 -->
                   <div class="tab-pane" id="tab_2">
                           <div class="portlet box red">
                               <div class="portlet-body form">
                                   <!-- BEGIN FORM-->
                                   
                                <!-- END FORM-->
                            	</div>
                        	</div>
                   </div>
                   <!-- end tab_2 -->
                   <div class="tab-pane" id="tab_3">
                         <div class="portlet box yellow">
                             <div class="portlet-body form">
                                 <!-- BEGIN FORM-->
                                 
                              <!-- END FORM-->
                          	</div>
                      	</div>
                   </div>
                   <!-- end tab_3 -->
 				</div>
 				<!-- end tab-content -->
 			</div>
 			<!-- end  tabbable-line-->
   	</div>
   	<!-- end col-md-12 -->
</div>
<!-- end row -->