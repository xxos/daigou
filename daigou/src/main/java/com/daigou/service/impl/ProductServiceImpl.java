package com.daigou.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.ProductMapper;
import com.daigou.dto.ProductQuery;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Product;
import com.daigou.service.ProductService;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: ProductServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("productService")  
public class ProductServiceImpl implements ProductService {
	@Autowired  
	protected ProductMapper productMapper;
	
	public ServiceResult insertOrUpdate(Product product){
		if(product.getId() == null || product.getId().equals(0L)){
			product.setId(GenerateUUID.generateUUID());
			productMapper.insertSelective(product);
		}else{
			productMapper.updateByPrimaryKeySelective(product);
		}
		return new ServiceResult(true,"商品保存成功!",product.getId());
	}
	
	public int deleteByPrimaryKey(Long id){
		return productMapper.deleteByPrimaryKey(id);
	}

	public int insert(Product record){
		return productMapper.insert(record);
	}

	public int insertSelective(Product record){
		return productMapper.insertSelective(record);
	}
	public List<Product> selectByShopId(Long shopId,Integer status,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		paramMap.put("status", status);
		List<Product> productList = productMapper.selectByShopId(paramMap);
		for(Product product:productList){
			product.setStatusName(product.getStatus());
		}
		return productList;
	}
	/**
	 * 分页查询
	 * @param productQuery
	 * @return
	 */
	public List<Product> selectByProductByPage(ProductQuery productQuery){
		List<Product> productList = productMapper.selectByProductByPage(productQuery);
		for(Product product:productList){
			product.setStatusName(product.getStatus());
		}
		return productList;
	}
	/**
	 * 查询草稿状态的product
	 * @param shopId
	 * @param searchKey
	 * @return
	 */
	public List<Product> selectCaogaoByShopId(Long shopId,String searchKey){
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("shopId", shopId);
		paramMap.put("searchKey", searchKey);
		List<Product> productList =  productMapper.selectCaogaoByShopId(paramMap);
		for(Product product:productList){
			product.setStatusName(product.getStatus());
		}
		return productList;
	}
	/**
     * 查总数（不分页）
     * @param shopId
     * @return
     */
    public Integer selectOnSaleCountByShopId(Long shopId){
    	return productMapper.selectOnSaleCountByShopId(shopId);
    }
	/**
	 * 总数
	 */
	public Integer selectTotalCountByPage(ProductQuery productQuery){
		productQuery.setStatus(null);
		return productMapper.selectTotalCountWithStatusByPage(productQuery);
	}
	/**
	 * 上架
	 */
	public Integer selectOnSaleCountByPage(ProductQuery productQuery){
		productQuery.setStatus(1);
		return productMapper.selectTotalCountWithStatusByPage(productQuery);
	}
	/**
	 * 草稿
	 */
	public Integer selectCaogaoCountByPage(ProductQuery productQuery){
		return productMapper.selectCaogaoCountByPage(productQuery);
	}
	/**
	 * 下架
	 */
	public Integer selectOffSaleCountByPage(ProductQuery productQuery){
		productQuery.setStatus(0);
		return productMapper.selectTotalCountWithStatusByPage(productQuery);
	}
	
	public Product selectByPrimaryKey(Long id){
		Product product = productMapper.selectByPrimaryKey(id);
		if(product != null){
			product.setStatusName(product.getStatus());
		}
		return product;
	}
	/**
	 * 购物车商品列表
	 * @param ids
	 * @return
	 */
	public List<Product> selectByIds(String ids){
		return productMapper.selectByIds(ids);
	}

	public int updateByPrimaryKeySelective(Product record){
		return productMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKeyWithBLOBs(Product record){
		return productMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	public int updateByPrimaryKey(Product record){
		return productMapper.updateByPrimaryKey(record);
	}
}  