<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.daigou.dao.UserMapper" >
	<resultMap id="BaseResultMap" type="com.daigou.model.User" >
    	<id column="id" property="id" jdbcType="BIGINT" />
    	<id column="shop_id" property="shopId" jdbcType="BIGINT" />
    	<result column="login_name" property="loginName" jdbcType="VARCHAR" />
    	<result column="name" property="name" jdbcType="VARCHAR" />
    	<result column="pwd" property="pwd" jdbcType="VARCHAR" />
		<result column="status" property="status" jdbcType="INTEGER" />
		<result column="email" property="email" jdbcType="VARCHAR" />
		<result column="mobile" property="mobile" jdbcType="VARCHAR" />
		<result column="sex" property="sex" jdbcType="VARCHAR" />
		<result column="id_card" property="idCard" jdbcType="VARCHAR" />
		<result column="weixin" property="weixin" jdbcType="VARCHAR" />
		<result column="zhifubao" property="zhifubao" jdbcType="VARCHAR" />
		<result column="qq" property="qq" jdbcType="VARCHAR" />
		<result column="weixin_qr_code" property="weixinQrCode" jdbcType="VARCHAR" />
		<result column="zhifubao_qr_code" property="zhifubaoQrCode" jdbcType="VARCHAR" />
		<result column="qq_qr_code" property="qqQrCode" jdbcType="VARCHAR" />
		<result column="creator_Id" property="creatorId" jdbcType="BIGINT" />
		<result column="created_Time" property="createdTime" jdbcType="TIMESTAMP" />
		<result column="leader_Id" property="leaderId" jdbcType="BIGINT" />
		<result column="leader_name" property="leaderName" jdbcType="VARCHAR" />
	</resultMap>
	<sql id="Base_Column_List" >
		id,
		shop_id, 
		login_name,
		name,
		pwd,
		mobile,
		status,
		type,
		email,
		sex,
		id_card,
		address,
		weixin,
		weixin_qr_code,
		zhifubao,
		zhifubao_qr_code,
		qq,qq_qr_code,
		leader_Id,
		leader_name,
    	creator_Id,
    	created_Time
		
	</sql>
  	<select id="login" resultMap="BaseResultMap">
		select <include refid="Base_Column_List" /> from tb_user where login_name = #{0} and pwd = #{1}
	</select>
    <insert id="register" parameterType="com.daigou.model.User">
	    insert into tb_user (id,shop_id, login_name,name,address, pwd,email,mobile,type,sex,status,
	    leader_id,leader_name,
	    creator_Id,created_Time)
	    	values
	    (#{id,jdbcType=BIGINT}, 
	    #{shopId,jdbcType=BIGINT},
	    #{loginName,jdbcType=VARCHAR},
	    #{name,jdbcType=VARCHAR},
	    #{address,jdbcType=VARCHAR},
	    #{pwd,jdbcType=VARCHAR}, 
	    #{email,jdbcType=VARCHAR},
	    #{mobile,jdbcType=BIGINT},
	    #{type,jdbcType=INTEGER},
	    #{sex,jdbcType=INTEGER},
	    #{status,jdbcType=CHAR},
	    #{leaderId,jdbcType=BIGINT},
	    #{leaderName,jdbcType=VARCHAR},
	    #{creatorId,jdbcType=BIGINT},
	    #{createdTime,jdbcType=TIMESTAMP})
	</insert>
	<insert id="insertEmployee" parameterType="com.daigou.model.User">
	    insert into tb_user (id,shop_id, 
	    	login_name,name,address, pwd,
	    	email,mobile,type,sex,status,
	    	leader_Id,leader_name,
	    	creator_Id,created_Time)
    	values
	    (
		    #{id,jdbcType=BIGINT}, 
		    #{shopId,jdbcType=BIGINT},
		    #{loginName,jdbcType=VARCHAR},
		    #{name,jdbcType=VARCHAR},
		    #{address,jdbcType=VARCHAR},
		    #{pwd,jdbcType=VARCHAR}, 
		    #{email,jdbcType=VARCHAR},
		    #{mobile,jdbcType=BIGINT},
		    #{type,jdbcType=INTEGER},
		    #{sex,jdbcType=INTEGER},
		    #{status,jdbcType=CHAR},
		    #{leaderId,jdbcType=BIGINT},
		    #{leaderName,jdbcType=VARCHAR},
		    #{creatorId,jdbcType=BIGINT},
		    #{createdTime,jdbcType=TIMESTAMP}
	    )
	</insert>
	<select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="java.lang.Long" >
    	select 
    	<include refid="Base_Column_List" />
    	from tb_user where id = #{uId}
  	</select>
  	<select id="selectAdminUser" resultMap="BaseResultMap" parameterType="java.lang.Long" >
    	select 
    	<include refid="Base_Column_List" />
    	from tb_user where shop_id = #{shopId} and type = 1
  	</select>
  	<select id="selectByLoingName" resultMap="BaseResultMap" parameterType="java.lang.String" >
    	select
		<include refid="Base_Column_List" />
    	from tb_user where login_name = #{loginName}
  	</select>
  	<select id="selectByEmail" resultMap="BaseResultMap" parameterType="java.lang.String" >
    	select * from tb_user where email = #{email}
  	</select>
  	<update id="updatePasswordByEmail">
    	update tb_user set pwd = #{1} where email = #{0}
	</update>
	<update id="updatePasswordById">
    	update tb_user set pwd = #{1} where id = #{0}
	</update>
	<select id="selectAllUser" resultMap="BaseResultMap">
		select <include refid="Base_Column_List" /> from tb_user
	</select>
  	<update id="updateByPrimaryKeySelective" parameterType="com.daigou.model.User" >
    	update tb_user
    	<set>
    		<if test="shopId != null" >
        		shop_id = #{shopId,jdbcType=BIGINT},
      		</if>
    		<if test="name != null" >
        		name = #{name,jdbcType=VARCHAR},
      		</if>
    		<if test="loginName != null" >
        		login_name = #{loginName,jdbcType=VARCHAR},
      		</if>
      		<if test="mobile != null" >
        		mobile = #{mobile,jdbcType=BIGINT},
      		</if>
      		<if test="status != null" >
        		status = #{status,jdbcType=INTEGER},
      		</if>
      		<if test="type != null" >
        		type = #{type,jdbcType=INTEGER},
      		</if>
      		<if test="email != null" >
        		email = #{email,jdbcType=VARCHAR},
      		</if>
      		<if test="pwd != null" >
        		pwd = #{pwd,jdbcType=VARCHAR},
      		</if>
      		<if test="weixin != null" >
        		weixin = #{weixin,jdbcType=VARCHAR},
      		</if>
      		<if test="weixinQrCode != null" >
        		weixin_qr_code = #{weixinQrCode,jdbcType=VARCHAR},
      		</if>
      		<if test="zhifubao != null" >
        		zhifubao = #{zhifubao,jdbcType=VARCHAR},
      		</if>
      		<if test="zhifubaoQrCode != null" >
        		zhifubao_qr_code = #{zhifubaoQrCode,jdbcType=VARCHAR},
      		</if>
      		<if test="qq != null" >
        		qq = #{qq,jdbcType=VARCHAR},
      		</if>
      		<if test="qqQrCode != null" >
        		qq_qr_code = #{qqQrCode,jdbcType=VARCHAR},
      		</if>
      		<if test="sex != null" >
        		sex = #{sex,jdbcType=VARCHAR},
      		</if>
      		<if test="address != null" >
        		address = #{address,jdbcType=VARCHAR},
      		</if>
      		<if test="idCard != null" >
        		id_card = #{idCard,jdbcType=VARCHAR},
      		</if>
      		<if test="leaderId != null" >
        		leader_id = #{leaderId,jdbcType=BIGINT},
      		</if>
      		<if test="leaderName != null" >
        		leader_name = #{leaderName,jdbcType=VARCHAR},
      		</if>
      		<if test="modifierId != null">
		        modifier_id = #{modifierId,jdbcType=BIGINT},
	        </if>
	        <if test="modifiedTime != null">
	        	modified_time = #{modifiedTime,jdbcType=TIMESTAMP},
	        </if>
    	</set>
    	where id = #{id,jdbcType=BIGINT}
  	</update>
  	<select id="selectMyEmployee" resultMap="BaseResultMap" parameterType="java.util.Map">
    	select 
    	<include refid="Base_Column_List" />
    	from tb_user where 
    	shop_id = #{shopId,jdbcType=BIGINT} 
    	<if test="type != null and type =='23'">
	    	and type in(2,3)
	    </if>
	    <if test="type != null and type !='23'">
	    	and type = #{type,jdbcType=INTEGER}
	    </if>
    	<if test="status != null">
	    	and status = #{status,jdbcType=INTEGER}
	    </if>
	    <if test="searchKey != null and searchKey !=''">
		    and (
			   	name like concat('%',concat(#{searchKey,jdbcType=VARCHAR},'%'))
			   	or
			    login_name like concat('%',concat(#{searchKey,jdbcType=VARCHAR},'%'))
		    )
	    </if>
  	</select>
</mapper>