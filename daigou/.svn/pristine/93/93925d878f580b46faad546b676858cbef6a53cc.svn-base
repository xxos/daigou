package com.daigou.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.OrderQuery;
import com.daigou.dto.ServiceResult;
import com.daigou.enums.OrderStatusEnum;
import com.daigou.model.Order;
import com.daigou.model.Product;
import com.daigou.model.Shop;
import com.daigou.model.User;
import com.daigou.service.OrderService;
import com.daigou.service.ProductService;
import com.daigou.service.ShopService;
import com.daigou.service.UserService;
import com.daigou.tools.GenerateUUID;

@Controller
public class OrderController {
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopService shopService;
	
	@Autowired
    private UserService userService;
	/**
	 * 购物车
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/order/product/{id}/shop-car",method = RequestMethod.GET)
    public String toShoppingCarPage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		LOGGER.info("进入购物车{}",id);
		Product product = productService.selectByPrimaryKey(id);
		if(product != null){
			Order order = new Order();
			order.setProductId(product.getId());
			order.setProductName(product.getName());
			order.setSpec(product.getSpec());
			order.setCate(product.getCate());
			order.setBrand(product.getBrand());
			order.setProductionPlace(product.getProductionPlace());
			order.setPicMain(product.getPicMain());
			order.setPrice(product.getSalePrice());
			order.setTotalAmount(product.getSalePrice());
			//新建待确认
			order.setStatus(OrderStatusEnum.NEW_UNCONFIRM.getCode());
			//草稿
			order.setDr(0);
			//orderService.insertSelective(order);
			model.addAttribute("order", order);
		}
		LOGGER.info("进入购物车{}",id);
		return "/shop/app/shop_car";
    }
	/**
	 * 立即购买
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/order/product/{id}/go-buy",method = RequestMethod.GET)
    public String toOrderPage(HttpServletResponse response,Model model,@PathVariable("id")Long id){
		LOGGER.info("进入立即购买{}",id);
		Product product = productService.selectByPrimaryKey(id);
		if(product != null){
			Order order = new Order();
			//立即购买，已经有订单号了，给到前端
			order.setId(GenerateUUID.generateUUID());
			order.setProductId(product.getId());
			order.setProductName(product.getName());
			order.setSpec(product.getSpec());
			order.setCate(product.getCate());
			order.setBrand(product.getBrand());
			order.setProductionPlace(product.getProductionPlace());
			order.setPicMain(product.getPicMain());
			order.setPrice(product.getSalePrice());
			order.setTotalAmount(product.getSalePrice());
			//新建待确认
			order.setStatus(OrderStatusEnum.NEW_UNCONFIRM.getCode());
			//0，未支付
			order.setPayStatus(0);
//			//设置cookie存储订单的id
//			Cookie cookie = new Cookie("go-buy-order-id",order.getId().toString());//创建新cookie
//	        cookie.setMaxAge(24 * 60);// 设置存在时间为5分钟
//	        cookie.setPath("/");//设置作用域
//	        response.addCookie(cookie);//将cookie添加到response的cookie数组中返回给客户端
			model.addAttribute("order", order);
			model.addAttribute("product", product);
		}
		LOGGER.info("进入订单{}",id);
		return "/shop/app/order";
    }
	/**
	 * 提交订单
	 * @return
	 */
	@RequestMapping(value = "/order",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult order(HttpServletRequest request,@RequestBody Order order){
		Object shopObj = request.getSession().getAttribute(CommonConstants.SHOP_INFO);
		Shop shop = null;
		if(shopObj == null){
			shop = shopService.selectByPrimaryKey(order.getShopId());
			request.getSession().setAttribute(CommonConstants.SHOP_INFO, shop);
		}else{
			shop = (Shop)shopObj;
		}
		order.setSalerId(shop.getSalerId());
		order.setShopId(shop.getId());
		order.setShopName(shop.getName());
		order.setCreatorId(shop.getSalerId());
		order.setCreatedTime(new Date());
		try{
			ServiceResult serviceResult = orderService.insertOrUpdate(order);
	        return new AjaxResult(serviceResult);
		}catch(Exception ex){
			return new AjaxResult(false,ex.getMessage());
		}
    }
	/**
	 * 订单列表
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{shopCode}/{loginName}/order/list",method = RequestMethod.GET)
    public String toMyOrderListPage(HttpServletRequest request,Model model,
    		@PathVariable("shopCode")String shopCode,
    		@PathVariable("loginName")String loginName){
		LOGGER.info("进入订单列表{}",loginName);
		User userDb = userService.selectByLoingName(loginName);
		Object shopObj = request.getSession().getAttribute(CommonConstants.SHOP_INFO);
		Shop shop = null;
		if(shopObj == null){
			shop = shopService.selectByCode(shopCode);
			request.getSession().setAttribute(CommonConstants.SHOP_INFO, shop);
		}else{
			shop = (Shop)shopObj;
		}
		OrderQuery orderQuery = new OrderQuery();
		orderQuery.setShopId(shop.getId());
		orderQuery.setCreatorId(userDb.getId());
		orderQuery.setStatus("678");
		orderQuery.setPayStatus("123");
		List<Order> orderList = orderService.selectByCondition(orderQuery);
		List<Order> orderList1 = new ArrayList<Order>();
		List<Order> orderList2 = new ArrayList<Order>();
		List<Order> orderList4 = new ArrayList<Order>();
		List<Order> orderList5 = new ArrayList<Order>();
		for(Order order:orderList){
			//新建待确认
			if(order.getStatus().equals(OrderStatusEnum.NEW_UNCONFIRM.getCode())){
				orderList1.add(order);
			}
			//待支付和支付中的
			else if(order.getPayStatus().equals(1)||order.getPayStatus().equals(0)){
				orderList2.add(order);
			}
			//待收货
			else if(order.getStatus().equals(OrderStatusEnum.SEND_RECEIVE.getCode())){
				orderList4.add(order);
			}
			//其他
			else{
				orderList5.add(order);
			}
		}
		model.addAttribute("orderList", orderList);
		model.addAttribute("orderList1", orderList1);
		model.addAttribute("orderList2", orderList2);
		model.addAttribute("orderList4", orderList4);
		model.addAttribute("orderList5", orderList5);
		return "/shop/app/my_order";
    }
}
