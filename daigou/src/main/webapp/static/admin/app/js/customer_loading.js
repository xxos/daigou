//动态分页加载
function loaded () {
    var myScroll,
        upIcon = $("#up-icon"),
        downIcon = $("#down-icon"),
        distance = 30; //滑动距离
  //重设高度
    var detailBoxDivHeight = $('#detailBoxDiv').height();
    //var browser_width = $(document.body).height();
    if(detailBoxDivHeight <= 440){
    	$("#detailBoxDiv").css("minHeight","440px");
    }
    myScroll = new IScroll('#wrapper', { probeType: 3, mouseWheel: true, click:true });
    	
    myScroll.on("scroll",function(){
        var y = this.y,
            maxY = this.maxScrollY - y,
            downHasClass = downIcon.hasClass("reverse_icon"),
            upHasClass = upIcon.hasClass("reverse_icon");
        if(y >= distance){
            !downHasClass && downIcon.addClass("reverse_icon");
            return "";
        }else if(y < distance && y > 0){
            downHasClass && downIcon.removeClass("reverse_icon");
            return "";
        }

        if(maxY >= distance){
            !upHasClass && upIcon.addClass("reverse_icon");
            return "";
        }else if(maxY < distance && maxY >=0){
            upHasClass && upIcon.removeClass("reverse_icon");
            return "";
        }
    });
    var pageIndex=1;
    var pageSize=20;
    function upAjax(){
    	//获取form的值
		var searchKey = $('#searchInput').val();
		var customerQuery = {};
		customerQuery["searchKey"] = searchKey;
		
		pageIndex++;
		customerQuery["pageIndex"] = pageIndex;
		customerQuery["pageSize"] = pageSize;
        var params = JSON.stringify(customerQuery);
        $.ajax({
            type: "post",
            url:baselocation+'/customer-query',
            data: params,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(function(result) {
            if (result && result.success && result.data.rows.length > 0) {
            	var totalCount = result.data.total;
				
            	var customerList = result.data.rows;
				var jsonStr = "";
				if(customerList.length > 0){
					for(var i=0;i<customerList.length;i++){
						var customer = customerList[i];
						jsonStr += "<a href='"+baselocation+"/customer/"+customer.id+"/details' class='details-link'>";
						jsonStr += "<ul class='details-list'>";
						jsonStr += "<li>"+ customer.customerName +"</li>";
						jsonStr += "<li>"+customer.customerLevel+"</li>";
						jsonStr += "<li>"+customer.mobile+"</li>";
						jsonStr += "<li>"+customer.businessName+"</li>";
						jsonStr += "</ul>";
						jsonStr += "</a>";
					}
				}
				
                $('#detailBoxDiv').append(jsonStr);
                myScroll.refresh(totalCount);
            }else{
            	$("#pullUp-msg").html("别拉了,到底了,没有更多数据了！！！");
            }
        }).fail(function() {
        	$("#detailBoxDiv").html("<font style='font-size:0.16rem;'>数据请求失败，请重新刷新</font>");
        })
    }

    function downAjax(){
    	//获取form的值
		var searchKey = $('#searchInput').val();
		var customerQuery = {};
		pageIndex = 1;
		customerQuery["pageIndex"] = pageIndex;
		customerQuery["pageSize"] = pageSize;
        var params = JSON.stringify(customerQuery);
        $.ajax({
            type: "post",
            url: baselocation+'/customer-query',
            data: params,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).done(function(result) {
        	if (result && result.success && result.data.rows.length > 0) {
            	var totalCount = result.data.total;
            	var customerList = result.data.rows;
				var jsonStr = "";
				if(customerList.length > 0){
					for(var i=0;i<customerList.length;i++){
						var customer = customerList[i];
						jsonStr += "<a href='"+baselocation+"/customer/"+customer.id+"/details' class='details-link'>";
						jsonStr += "<ul class='details-list'>";
						jsonStr += "<li>"+ customer.customerName +"</li>";
						jsonStr += "<li>"+customer.customerLevel+"</li>";
						jsonStr += "<li>"+customer.mobile+"</li>";
						jsonStr += "<li>"+customer.businessName+"</li>";
						jsonStr += "</ul>";
						jsonStr += "</a>";
					}
				}
				
                $('#detailBoxDiv').html(jsonStr);
                $('#customerTitle').html("客户列表("+totalCount+")");
                myScroll.refresh(totalCount);
            }
        }).fail(function() {
        	$("#detailBoxDiv").html("<font style='font-size:0.16rem;'>数据请求失败，请重新刷新</font>");
        })
    }
    // 下拉刷新事件
    myScroll.on("slideDown",function(){
        if(this.y > distance){
        	$("#pullUp-msg").html("上拉加载更多");
            downAjax();
            upIcon.removeClass("reverse_icon")
        }
    });
    // 上拉滑动事件
    myScroll.on("slideUp",function(){
    	console.log(this.maxScrollY);
    	console.log(this.y);
        if(this.maxScrollY - this.y > distance){
            upAjax();
            upIcon.removeClass("reverse_icon")
        }
    });
}
loaded ();