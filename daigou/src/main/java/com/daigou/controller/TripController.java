package com.daigou.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.daigou.model.Shop;
import com.daigou.model.Trip;
import com.daigou.service.ProductService;
import com.daigou.service.ShopService;
import com.daigou.service.TripService;

@Controller
public class TripController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TripController.class);
	@Autowired
	private TripService tripService;
	@Autowired
	private ProductService productService;
	@Autowired
	private ShopService shopService;
	@RequestMapping(value = "/trip/{id}")
    public String trip(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		LOGGER.info("进入广告页");
		Trip trip = tripService.selectByPrimaryKey(id);
		model.addAttribute("trip", trip);
		Shop shop = shopService.selectByPrimaryKey(trip.getShopId());
		model.addAttribute("shop", shop);
		//在销售商品数量
		Integer onSaleCount = productService.selectOnSaleCountByShopId(shop.getId());
		model.addAttribute("onSaleCount", onSaleCount);
		return "/shop/app/advert";
	}
}
