package com.daigou.admin.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.dto.AjaxResult;
import com.daigou.model.User;
import com.daigou.service.UserService;

/**
 * 
 * 类名称：LoginController   
 * 创建人：李志   
 * 创建时间：2018年02月14日  上午12:16:42  
 * 修改人：李志   
 * 修改时间：2018年04月14日 00:35:17   
 * @version    
 *
 */
@Controller
public class LoginController{
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/admin/login",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult login(@RequestBody User user){
		LOGGER.info("开始登录");
		User userDb = userService.login(user.getLoginName(), user.getPwd());
		if(userDb == null){
			return new AjaxResult(false,"用户名或密码错误!");
		}
		return new AjaxResult(true,"登录成功!",userDb);
    }
	
	@RequestMapping(value = "/admin/login",method = RequestMethod.GET)
    public String toLoginPage(){
		LOGGER.info("进入登录页面");
        return "/admin/app/login";
    }
	
	@RequestMapping(value = "/admin/register",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult register(@RequestBody User user){
		LOGGER.info("开始注册");
		//0,超级管理员,1，卖家，2，买家
		//从此处注册的，类型为1
		user.setType(1);
		userService.register(user);
        return new AjaxResult(true,"注册成功!",user);
    }
	
	@RequestMapping(value = "/admin/register",method = RequestMethod.GET)
    public String toRegisterPage(){
		LOGGER.info("进入注册页面");
        return "/admin/app/register";
    }
}
