//表单验证
$(function(){
	function listCustomer(){
		//获取form的值
		var searchKey = $('#searchInput').val();
		var customerQuery = {};
		customerQuery["searchKey"] = searchKey;
		
		customerQuery["pageIndex"] = 1;
		customerQuery["pageSize"] = 20;
        var params = JSON.stringify(customerQuery);
		
		$.ajax({
			url:baselocation+'/customer-query',
			contentType:'application/json',
			type:'post',
			dataType:'json',
			data: params,
			success:function(result){
				if(result.success==false){
					layer.msg(result.message,{icon:2,time:2000});
				}else{
					var customerList = result.data.rows;
					var jsonStr = "";
					if(customerList.length > 0){
						for(var i=0;i<customerList.length;i++){
							var customer = customerList[i];
							jsonStr += "<a href='"+baselocation+"/customer/"+customer.id+"/details' class='details-link'>";
							jsonStr += "<ul class='details-list'>";
							jsonStr += "<li>"+ customer.customerName +"</li>";
							jsonStr += "<li>"+customer.customerLevel+"</li>";
							jsonStr += "<li>"+customer.mobile+"</li>";
							jsonStr += "<li>"+customer.businessName+"</li>";
							jsonStr += "</ul>";
							jsonStr += "</a>";
						}
					}
					
					$("#detailBoxDiv").html(jsonStr);
				}
			},
			error:function(result){
				alert("系统异常,请联系管理员！");
			}
		});
	}
	
	$('#searchBtn').click(function () {
		listCustomer();
	})
	$('#searchInput').blur(function () {
		listCustomer();
	})
});