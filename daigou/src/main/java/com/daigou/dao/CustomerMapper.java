package com.daigou.dao;

import java.util.List;

import com.daigou.dto.CustomerQuery;
import com.daigou.model.Customer;

public interface CustomerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Customer record);

    int insertSelective(Customer record);
    
    Customer selectByMobile(Long mobile);

    Customer selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);
    
    public List<Customer> selectShopCustomer(CustomerQuery customerQuery);
}