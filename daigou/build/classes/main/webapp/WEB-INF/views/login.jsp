<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>微微云简历</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <jsp:include page="/app/global_css.jsp"></jsp:include>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
	    <link href="<%=basePath%>/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
	    <link href="<%=basePath%>/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
	    <!-- END PAGE LEVEL PLUGINS -->
	    <!-- BEGIN PAGE LEVEL STYLES -->
	    <link href="<%=basePath%>/assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
	    <!-- END PAGE LEVEL STYLES -->
    </head>
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="index.html">
                <img src="<%=basePath%>/assets/pages/img/logo-big.png" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
	        <!-- begin 登录提示框 -->
			<div style="display:none;"><a href="#myModal2" role="button" id="btnerr" class="btn btn-danger" data-toggle="modal"></a></div>
			<div class="modal fade" id="myModal2" tabindex="-1" role="basic" aria-hidden="true">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                        <h3 style="color:black;">提示信息</h3>
	                    </div>
	                    <div class="modal-body">
	                    	<p id="infomsg" style="color:black;"></p>
						</div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn green" data-dismiss="modal">确定</button>
	                    </div>
	                </div>
	                <!-- /.modal-content -->
	            </div>
	            <!-- /.modal-dialog -->
	        </div>
			<!-- end 登录提示框 -->
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" id = "login-form">
                <h3 class="form-title">微微云简历</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="login_errmsg">请输入您的用户名和密码.</span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">用户名</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="用户名" name="loginName" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">密码</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="密码" name="pwd" /> </div>
                </div>
                <div id="showVerifyDiv" style="display: none;" class="form-group">
					<label class="control-label visible-ie8 visible-ie9">验证码</label>
					<div class="input-group">
						<div class="input-icon left">
							<i class="fa fa-tag"></i>
							<input class="form-control placeholder-no-fix" type="text" placeholder="验证码" name="verifyCode" style="width:100px;"/>
							<iframe src="<%=basePath%>/ajaxValidateCode" id="iframecode" style="height:34px; width:85px; margin: 0px;" frameborder="0" scrolling="no" ></iframe>
							<div style="float: right;">
								&nbsp;&nbsp;<button id="showcode"  class="btn blue"><i class="icon-refresh" style="margin:2px 8px;"></i></a>
							</div>
						</div>
					</div>
				</div>
                <div class="form-actions">
                    <label class="checkbox">
                        <input type="checkbox" name="remember" value="1" />记住我</label>
                    <button type="submit" class="btn green pull-right">登录</button>
                </div>
                <div class="login-options">
                    <h4>用其他账号登录</h4>
                    <ul class="social-icons">
                        <li>
                            <a class="facebook" data-original-title="facebook" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="twitter" data-original-title="Twitter" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="googleplus" data-original-title="Goole Plus" href="javascript:;"> </a>
                        </li>
                        <li>
                            <a class="linkedin" data-original-title="Linkedin" href="javascript:;"> </a>
                        </li>
                    </ul>
                </div>
                <div class="forget-password">
                    <h4>忘记密码?</h4>
                    <p>不用担心,点击<a href="javascript:;" id="forget-password">这里</a>找回你的密码.</p>
                </div>
                <div class="create-account">
                    <p>还没有账号?&nbsp;
                        <a href="javascript:;" id="register-btn">注册账号</a>
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" id = "forget-form">
                <h3>忘记密码?</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="forget_errmsg"></span>
                </div>
                <p>输入您的电子邮件地址重置您的密码.</p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="请输入您注册时的邮箱" name="findPwdEmail" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">返回 </button>
                    <button type="submit" class="btn green pull-right"> 提交 </button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
            <!-- BEGIN REGISTRATION FORM -->
            <form class="register-form" id = "register-form">
                <h3>注册</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="register_errmsg"></span>
                </div>
                <p>在下面输入您的帐户资料:</p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">用户名</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="用户名" id="register_loginName" name="loginName"/> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">密码</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="密码" name="pwd"/> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">请重新输入您的密码</label>
                    <div class="controls">
                        <div class="input-icon">
                            <i class="fa fa-check"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="重复密码" name="rpassword" /> </div>
                    </div>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">邮箱</label>
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="请输入邮箱,以接收系统邮件(重要)" id="register_email" name="email" /> </div>
                </div>
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="tnc" />我同意
                        <a href="javascript:;">服务条件</a>和
                        <a href="javascript:;">隐私政策</a>
                    </label>
                    <div id="register_tnc_error"> </div>
                </div>
                <div class="form-actions">
                    <button id="register-back-btn" type="button" class="btn red btn-outline">返回</button>
                    <button type="submit" id="register-submit-btn" class="btn green pull-right">注册</button>
                </div>
            </form>
            <!-- END REGISTRATION FORM -->
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
	<div class="copyright">2016 &copy; Party - 微微云简历平台</div>
	<!-- END COPYRIGHT -->
	<jsp:include page="/app/global_js.jsp"></jsp:include>
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<%=basePath%>/app/js/login-4.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
     <script src="<%=basePath%>/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
     <script src="<%=basePath%>/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
     <script src="<%=basePath%>/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
     <script src="<%=basePath%>/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
     <!-- END PAGE LEVEL PLUGINS -->
	<script>
		jQuery(document).ready(function() {
		  var baselocation='<%=basePath%>';
		  App.init();
		  Login.init(baselocation);
		});
		$(function(){
			var baselocation='<%=basePath%>';
			$("#showcode").click(function(){
				var url="<%=basePath%>/ajaxValidateCode?v="+new Date().valueOf();
				window.parent.frames[0].location.href=url;
			});
			
			var err='${err}';
			if(err!=''){
				$("#infomsg").text(err);
				$("#btnerr").click();
			}
			var showVerify='${showVerify}';
			if(showVerify!=''){
				$("#showVerifyDiv").show();
			}
			$('#register_loginName').blur(function() {
				var str = $(this).val();
				$.ajax({
					url :"checkSameName",
					type : "post",
					dataType : "json",
					data : {"loginName":str},
					success : function(result) {
						if (result.success == false){
							$("#register_errmsg").text(result.message);
							$('.alert-danger', $('.register-form')).show();
							$("#register_loginName").val();
							$("#register_loginName").focus();
						}else{
							$("#register_errmsg").text();
							$('.alert-danger', $('.register-form')).hide();
						}
					}
				});
			});
			$('#register_email').blur(function() {
				var str = $(this).val();
				$.ajax({
					url :"checkSameEmail",
					type : "post",
					dataType : "json",
					data : {"email":str},
					success : function(result) {
						if (result.success == false){
							$("#register_errmsg").text(result.message);
							$('.alert-danger', $('.register-form')).show();
							$("#register_email").val();
							$("#register_email").focus();
						}else{
							$("#register_errmsg").text();
							$('.alert-danger', $('.register-form')).hide();
						}
					}
				});
			});
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>