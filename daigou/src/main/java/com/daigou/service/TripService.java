package com.daigou.service;

import java.util.List;

import com.daigou.model.Trip;

 

/**  
 * @Title: TripService.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
public interface TripService{
	
	Trip selectByPrimaryKey(Long id);
	
	public int insert(Trip record);

	public int updateByPrimaryKeySelective(Trip record);
	
	public List<Trip> selectByShopId(Long shopId);
}  