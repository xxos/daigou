<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
	<title>仿京东触屏版html5手机wap购物网站模板下载</title>
	<meta name="author" content="m.jd.com">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/shop/css/base2013.css" charset="gbk" />
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/static/shop/css/2013/sale/sale.css" charset="gbk" />
	<link rel="apple-touch-icon-precomposed" href="<%=basePath%>/static/shop/m.jd.com/images/apple-touch-icon.png" />
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/html5/common.js"></script>
	<script type="text/javascript" src="<%=basePath%>/static/shop/js/html5/spin.min.js"></script>
</head>
<body id="body">
	<a name="top"></a>
	<header>
		<div class="new-header">
			<a href="javascript:pageBack();" class="new-a-back" id="backUrl"><span>返回</span></a>
			<h2>代购小助手</h2>
			<a href="javascript:void(0)" id="btnJdkey" class="new-a-jd"><span>代购键</span></a>
		</div>
		<div class="new-jd-tab" style="display: none" id="jdkey">
			<div class="new-tbl-type">
				<a href="../index.html@sid=e0d0f025d6d3e8e8d7be5428c43ef911"
					class="new-tbl-cell"> <span class="icon">首页</span>
					<p style="color: #6e6e6e;">首页</p>
				</a> <a href="<%=basePath%>/productListM1"
					class="new-tbl-cell"> <span class="icon2">产品列表</span>
					<p style="color: #6e6e6e;">产品列表</p>
				</a> <a href="javascript:void(0)" id="html5_cart" class="new-tbl-cell">
					<span class="icon3">购物车</span>
					<p style="color: #6e6e6e;">购物车</p>
				</a> <a href="../user/home.action@sid=e0d0f025d6d3e8e8d7be5428c43ef911"
					class="new-tbl-cell"> <span class="icon4">我的订单</span>
					<p style="color: #6e6e6e;">我的订单</p>
				</a>
			</div>
		</div>
	</header>
	<input type="hidden" id="page" value="1" />
	<div class="new-ct">
		<div class="new-goods-lst">
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">ce-link 手机配件专场</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a href="<%=basePath%>/product"> <img src="<%=basePath%>/static/shop/images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M06/18/13/rBEhVFMRSnsIAAAAAAQdnleCvE4AAJRXQD3Lj8ABB22135.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-11-30</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">蝴蝶节-通讯</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a href="1.html"> <img src="<%=basePath%>/static/shop/images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M03/16/11/rBEhVFMHJZIIAAAAAAJ10Q8wbPIAAI3ugDsP_QAAnXp758.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-04-30</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">倍斯特品牌热卖-有良心的移动电源品牌，有良芯，才安心！</span> <span
						class="new-txt2">倍斯特品牌热卖-中国最有良心的移动电源品牌</span>
				</p>
				<div class="new-ad-img new-p-re">
					<a href="promotionActivityWare.action@activityId=41616&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="<%=basePath%>/static/shop/images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M05/11/06/rBEhV1MEVXkIAAAAAAGYqWehPbwAAI2VAGASoYAAZjB316.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">CE-LINK 让生活更简单</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=43058&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="<%=basePath%>/static/shop/images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M0A/18/01/rBEhU1MO5ncIAAAAAAQPHJFo6TQAAJKeQHHXMUABA80231.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-10-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">天津联通营业厅上线啦</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=43751&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M06/17/05/rBEhVVMfyXAIAAAAAAFxqN4ugiEAAKA7AHFexMAAXHA086.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-05-01</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">移动充电宝开学季</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=43639&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g15/M04/0A/07/rBEhWFMWm6MIAAAAAANHIk5jxS4AAJdLgB8TmwAA0c6826.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">白条私定制-通讯</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42388&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M06/17/06/rBEhUlMLHqoIAAAAAAJEueJmg9oAAI_1QMIx9oAAkTR026.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">移动4G百变精彩</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42705&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M09/12/1F/rBEhVVMNh64IAAAAAAFZxlsY18AAAJMjQA6F-kAAVne792.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">带上蓝牙去春游</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42704&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M02/19/10/rBEhVVMpNyQIAAAAAAIiAo28ux4AAKeRgOCD2wAAiIa754.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">第十九期-一一千个换机的理由-杀死比尔-2014杀手级手机致命出击</span> <span
						class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=30416&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M0A/19/17/rBEhU1MWtx0IAAAAAAG3TJEQ3ecAAJflAAajfcAAbdk015.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">2014.02.20开学季</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42715&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M03/17/16/rBEhVFMNi9IIAAAAAAGA952GPtoAAJGCwNl5s8AAYEP564.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-06-30</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">品牌特卖，全场疯抢，comma精品配件</span> <span
						class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=40428&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M03/0E/06/rBEhV1LgkAIIAAAAAAKXqH7XfmsAAIQ0AGvBPUAApfA206.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">北京联通沃4G上线啦</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=46876&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M06/1E/06/rBEhU1MpNf4IAAAAAAFEzwlvT-8AAKXVwNuL5cAAUTn120.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-05-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">京东酷派百亿战略合作</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=45456&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M03/17/05/rBEhV1MfyhsIAAAAAAEPaBgsIxsAAKA8AE-jV0AAQ-A786.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-05-01</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">电源品质月</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=45334&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M01/1B/15/rBEhVFMeyo0IAAAAAAEETy5vfGkAAJ3ogL1w-gAAQRn229.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-04-01</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">CE-LINK 手机配件专场</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=43140&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M08/13/05/rBEhVlMOifoIAAAAAAbRIAq3NWwAAJPFwKbcNAABtE4301.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-11-30</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">TD手机精品特惠</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42645&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M00/14/1F/rBEhV1MWmtwIAAAAAAOEyPYYWnIAAJlvwAqSNoAA4Tg338.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">羽博移动电源品牌秀场</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=40272&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g14/M08/0E/14/rBEhVlLmGFYIAAAAAAM_WB6o-n4AAIWlQDSPRIAAz9w506.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-05-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">雷曼克斯酬宾季</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=42386&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M05/17/04/rBEhVFMLAIoIAAAAAAMuLwXzq1kAAI_LADk6wUAAy5H230.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-03-31</span>
					</a>
				</div>
			</div>
			<div class="new-goods-ad">
				<p class="new-ad-cont">
					<span class="new-txt">羽博移动电源品牌特卖</span> <span class="new-txt2"></span>
				</p>
				<div class="new-ad-img new-p-re">
					<a
						href="promotionActivityWare.action@activityId=39854&module=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911">
						<img src="../images/touch2013/no_308_108.png"
						imgsrc="http://img30.360buyimg.com/mobilecms/s300x112_g13/M01/12/18/rBEhVFLfQQMIAAAAAAQbKOowRvUAAIIdwMMZE8ABBtA479.jpg"
						width="308" height="100" alt=""> <span>截止日期：2014-05-31</span>
					</a>
				</div>
			</div>
		</div>
		<div class="new-paging">
			<div class="new-tbl-type">
				<div class="new-tbl-cell">
					<span class="new-a-prve"><span>上一页</span></span>
				</div>
				<div class="new-tbl-cell new-p-re">
					<div class="new-a-page">
						<span class="new-open">1/2</span>
					</div>
					<select class="new-select"
						onChange="window.location.href=this.value">
						<option
							value="promotionActivity.action@promotionId=32&page=1&sid=e0d0f025d6d3e8e8d7be5428c43ef911"
							selected>第1页</option>
						<option
							value="promotionActivity.action@promotionId=32&page=2&sid=e0d0f025d6d3e8e8d7be5428c43ef911">第2页</option>
					</select>
				</div>
				<div class="new-tbl-cell">
					<a class="new-a-next"
						href='javascript:window.location.href="promotionActivity.action@promotionId=32&page=2&sid=e0d0f025d6d3e8e8d7be5428c43ef911"'><span>下一页</span></a>
				</div>
			</div>
		</div>
	</div>

	<div class="login-area" id="footer">
		<div class="login">
			<a href="../user/login.action@sid=e0d0f025d6d3e8e8d7be5428c43ef911">登录</a><span
				class="lg-bar">|</span><a
				href="../user/mobileRegister.action@sid=e0d0f025d6d3e8e8d7be5428c43ef911">注册</a>
			<span class="new-fr"><a
				href="../showvote.html@sid=e0d0f025d6d3e8e8d7be5428c43ef911">反馈</a><span
				class="lg-bar">|</span><a href="#top">回到顶部</a></span>
		</div>
		
		<div class="copyright">&copy; daigou.com</div>
	</div>

	<div style="display: none;">
		<img
			src="../ja.jsp@&utmn=752507904&utmr=-&utmp=_252Fpromotion_252FpromotionActivity.action_253Fsid_253De0d0f025d6d3e8e8d7be5048AA66116" />
	</div>
	<script type="text/javascript">
$("#unsupport").hide();
if(!testLocalStorage()){ //not support html5
    if(0!=0 && !$clearCart && !$teamId){
		$("#html5_cart_num").text(0>0>0);
	}
}else{
	updateToolBar('');
}

$("#html5_cart").click(function(){
//	syncCart('e0d0f025d6d3e8e8d7be5428c43ef911',true);
	location.href='../cart/cart.action';
});

function reSearch(){
var depCity = window.sessionStorage.getItem("airline_depCityName");
	if(testSessionStorage() && isNotBlank(depCity) && !/^\s*$/.test(depCity) && depCity!=""){
    	var airStr = '<form action="../airline/list.action" method="post" id="reseach">'
        +'<input type="hidden" name="sid"  value="e0d0f025d6d3e8e8d7be5428c43ef911"/>'
        +'<input type="hidden" name="depCity" value="'+ window.sessionStorage.getItem("airline_depCityName") +'"/>'
        +'<input type="hidden" name="arrCity" value="'+ window.sessionStorage.getItem("airline_arrCityName") +'"/>'
        +'<input type="hidden" name="depDate" value="'+ window.sessionStorage.getItem("airline_depDate") +'"/>'
        +'<input type="hidden" name="depTime" value="'+ window.sessionStorage.getItem("airline_depTime") +'"/>'
        +'<input type="hidden" name="classNo" value="'+ window.sessionStorage.getItem("airline_classNo") +'"/>'
        +'</form>';
    	$("body").append(airStr);
    	$("#reseach").submit();
	}else{
    	window.location.href='../airline/index.action@sid=e0d0f025d6d3e8e8d7be5428c43ef911';
	}
}
 //banner 关闭点击
    $('.div_banner_close').click(function(){
        $('#div_banner_header').unbind('click');
        jQuery.post('../index/addClientCookieVal.json',function(d){
              $('#div_banner_header').slideUp(500);
        });
    });
    //banner 下载点击
    $('.div_banner_download').click(function(){
         var downloadUrl = $(this).attr('url');
         jQuery.post('../index/addClientCookieVal.json',function(d){
               window.location.href=downloadUrl;
        });
    });
	
</script>
</body>
</html>
