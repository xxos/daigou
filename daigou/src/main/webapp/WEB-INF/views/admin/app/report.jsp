<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/base.css">
    <link rel="stylesheet" href="<%=basePath%>/static/admin/app/css/free-quota.css">
    <link href="<%=basePath%>/static/admin/app/css/iconfont.css" rel="stylesheet">
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery-1.11.3.min.js"></script>
    <title>财务报表</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-title">
        <a href="javascript:history.back(-1)" class="return"><i class="iconfont"></i>返回</a>
        <h2>我的报表</h2>
    </div>
</header>
<section>
    <div class="free-content">
        <div class="circle">
           <span class="circle-text">总营业额</span>
            <span class="circle-price"><small>¥</small>${orderAll.totalAmount }</span>
        </div>
    </div>
    <ul class="free-list">
        <li>
            <span class="text">本周</span>
            <span class="text-abs ">¥${orderWeek.totalAmount }</span>
        </li>
        <li>
            <span class="text">本月</span>
            <span class="text-abs ">¥${orderMonth.totalAmount }</span>
        </li>
        <li>
            <span class="text">本季度</span>
            <span class="text-abs ">¥${orderSeason.totalAmount }</span>
        </li>
        <li>
            <span class="text">半年内</span>
            <span class="text-abs ">¥${orderHalf.totalAmount }</span>
        </li>
        <li>
            <span class="text">今年</span>
            <span class="text-abs ">¥${orderYear.totalAmount }</span>
        </li>
    </ul>
    <h2 style="color:red">注：报表中的统计金额为所有支付状态为“确认收款”的订单</h2>
</section>
</div>
</body>
</html>