function isPhoneNo($poneInput) {
	var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
	if (!myreg.test($poneInput)) {
		return false;
	} else {
		return true;
	}
}
function isEmail(email){
	var szReg=/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
	var bChk=szReg.test(email);
	return bChk;
}
function isCode(code){
	var szReg=/^\w+$/;
	var bChk=szReg.test(code);
	return bChk;
}
