package com.daigou.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.daigou.dao.PayMapper;
import com.daigou.dto.ServiceResult;
import com.daigou.model.Order;
import com.daigou.model.Pay;
import com.daigou.service.OrderService;
import com.daigou.service.PayService;
import com.daigou.tools.GenerateUUID;

/**  
 * @Title: PayServiceImpl.java
 * @Package com.daigou.service
 * @Description: TODO
 * @author lizhi
 * @date 2018年3月27日
 */
@Service("payService")  
public class PayServiceImpl implements PayService {
	@Autowired  
	protected PayMapper payMapper;
	
	@Autowired
	private OrderService orderService;
	
	public Pay selectByPrimaryKey(Long id){
		Pay pay = payMapper.selectByPrimaryKey(id);
		pay.setPayMethodName(pay.getPayMethod());
		pay.setStatusName(pay.getStatus());
		return pay;
	}
	
	public int insert(Pay record){
		return payMapper.insert(record);
	}

	public ServiceResult insertPayAndUpdateOrder(Pay pay) throws Exception{
		if(pay.getId() == null || pay.getId().equals(0L)){
			pay.setId(GenerateUUID.generateUUID());
			payMapper.insertSelective(pay);
		}else{
			payMapper.updateByPrimaryKeySelective(pay);
		}
		Order order = orderService.selectByPrimaryKey(pay.getOrderId());
		BigDecimal payMoney = selectPayAmountByOrderId(order.getShopId(), order.getId());
		if(order.getNoPayAmount().compareTo(payMoney==null?BigDecimal.ZERO:payMoney) == 0){
			//已支付
			order.setPayStatus(2);
		}else{
			//支付中
			order.setPayStatus(1);
		}
		order.setId(pay.getOrderId());
		order.setPayAmount(pay.getPayMoney());
		order.setNoPayAmount(pay.getPayMoney());
		order.setModifiedTime(new Date());
		order.setModifierId(pay.getCreatorId());
		order.setNewTs(System.currentTimeMillis());
		order.setTs(order.getTs());
		if(!pay.getStatus().equals(0)){
			int result = orderService.updatePayAmountByPrimaryKey(order);
			if(result == 0){
				throw new Exception("支付失败,订单已过期,请刷新后重试!");
			}
		}
		return new ServiceResult(true,"支付成功!",order);
	}
	/**
	 * 为前端写的支付逻辑
	 * @param pay
	 * @return
	 * @throws Exception
	 */
	public ServiceResult insertPayAndUpdateOrder4Front(Pay pay) throws Exception{
		pay.setId(GenerateUUID.generateUUID());
		payMapper.insertSelective(pay);
		Order order = orderService.selectByPrimaryKey(pay.getOrderId());
		//支付中
		order.setPayStatus(1);
		order.setId(pay.getOrderId());
		order.setModifiedTime(new Date());
		order.setModifierId(pay.getCreatorId());
		order.setNewTs(System.currentTimeMillis());
		order.setTs(order.getTs());
		int result = orderService.updatePayAmountByPrimaryKey(order);
		if(result == 0){
			throw new Exception("支付失败,订单已过期,请刷新后重试!");
		}
		return new ServiceResult(true,"支付成功!",pay);
	}
	public int updateByPrimaryKeySelective(Pay record){
		return payMapper.updateByPrimaryKeySelective(record);
	}
	
	public List<Pay> selectByOrderId(Long shopId,Long orderId){
		List<Pay> payList = payMapper.selectByOrderId(shopId, orderId);
		for(Pay pay:payList){
			pay.setPayMethodName(pay.getPayMethod());
			pay.setStatusName(pay.getStatus());
		}
		return payList;
	}
	public BigDecimal selectPayAmountByOrderId(Long shopId,Long orderId){
		return payMapper.selectPayAmountByOrderId(shopId, orderId);
	}
}  