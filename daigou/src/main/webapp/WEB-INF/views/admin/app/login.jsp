<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>
<%@ include file="/WEB-INF/views/admin/app_base.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />
    <meta content="email=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@ include file="/WEB-INF/views/admin/app_js_css.jsp"%>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/jquery.cookie.js"></script>
    <script rel="script" src="<%=basePath%>/static/admin/app/js/register.js"></script>
    <title>${systemTitle}-登录</title>
</head>
<body>
<div class="viewport">
<header>
    <div class="header-bg">
        <div class="federal-text">
            <div class="tu-logo">
                <font size="7">${systemTitle}</font>
            </div>
        </div>
    </div>
</header>
<form>
    <div class="input-container login-name">
        <i class="iconfont"></i>
        <input id="loginName" type="text" class="acc-input"  placeholder="请输入手机号">
    </div>
    <div class="input-container pass-word">
        <i class="iconfont"></i>
        <input id="password" type="password" class="acc-input"  placeholder="请输入密码">
    </div>
    <div class="checkbox check-box">
        <label>
            <input type="checkbox" class="check-select" checked>
            <span class="auto-login">自动登录</span>
        </label>
    </div>
</form>
<div class="login-confirm">
    <a id="loginBtn" class="redBt">登录</a>
</div>
<div class="col-md-12">
	<a id="forgetPwdBtn"><small>忘记密码?</small></a>
</div>
<div class="col-md-12">
<p class="text-muted text-center"><small>还没有账号?</small>
<button id="tiyan" type="button" class="btn btn-success btn-xs">立即体验</button>
<a class="btn btn-sm btn-white btn-block" href="<%=basePath%>/admin/register">创建账号</a>
</div>
<div style="position: relative;text-align:center;margin:0 auto;top:5px;z-index:-100;">
	微信技术支持
</div>
<div style="position: relative;margin:0 auto;top:10px;border:1px solid;width:132px;height:132px;">
	<img alt="" src="<%=basePath%>/static/admin/app/images/lizhi_weixin_qr_code.png" width="130px" height="130px"/>
</div>
<div style="position: relative;margin:0 auto;top:30px;border:1px;width:180px;height:30px;">
	长按图片识别二维码加好友
</div>
</div>
</body>
</html>