package com.daigou.admin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daigou.constants.CommonConstants;
import com.daigou.dto.AjaxResult;
import com.daigou.dto.CustomerQuery;
import com.daigou.dto.PageInfo;
import com.daigou.dto.ServiceResult;
import com.daigou.dto.UserQuery;
import com.daigou.model.ShopCustomer;
import com.daigou.model.User;
import com.daigou.service.CustomerService;
import com.daigou.service.UserService;

@Controller("customerController")
@RequestMapping(value = "/admin")
public class CustomerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/customer",method = RequestMethod.GET)
    public String toCustomerPage(HttpServletRequest request,Model model){
		LOGGER.info("进入客户管理页面");
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		CustomerQuery customerQuery = new CustomerQuery();
		customerQuery.setShopId(user.getShopId());
		customerQuery.setUser(user);
		//设置分页
		customerQuery.buildPage("");
		PageInfo<ShopCustomer> pageInfo = customerService.selectShopCustomer(customerQuery);
		model.addAttribute("customerList", pageInfo.getRows());
		model.addAttribute("customerTotal", pageInfo.getTotal());
        return "/admin/app/customer_list";
    }
	/**
	 * 新增和编辑页
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/customer/{id}",method = RequestMethod.GET)
    public String toCustomerPage(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		ShopCustomer shopCustomer = null;
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		model.addAttribute("user", user);
		UserQuery userQuery = new UserQuery();
		userQuery.setShopId(user.getShopId());
		List<User> employeeList = userService.selectMyEmployee(userQuery);
		model.addAttribute("employeeList", employeeList);
		if(id == null || id.equals(0L)){
			LOGGER.info("进入客户新增{}",id);
			shopCustomer = new ShopCustomer();
			shopCustomer.setId(id);
			model.addAttribute("customerTitle", "客户新增");
		}else{
			LOGGER.info("进入客户更新{}",id);
			shopCustomer = customerService.selectByPrimaryKey(id);
			model.addAttribute("customerTitle", "客户编辑");
		}
		model.addAttribute("customer", shopCustomer);
        return "/admin/app/customer_update";
    }
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/customer/{id}",method = RequestMethod.DELETE)
	@ResponseBody
    public AjaxResult deleteCustomer(HttpServletRequest request,Model model,@PathVariable("id")Long id){
		if(id == null || id.equals(0L)){
			LOGGER.info("进入客户删除{}",id);
			return new AjaxResult(false,"删除失败,请传入客户id!");
		}else{
			LOGGER.info("进入客户更新{}",id);
			int result = customerService.deleteByPrimaryKey(id);
			if(result == 0){
				return new AjaxResult(false,"客户不存在,请刷新后重试!");
			}
			return new AjaxResult(true,"客户删除成功!");
		}
    }
	@RequestMapping(value = "/customer/{id}/details",method = RequestMethod.GET)
    public String toCustomerDetailsPage(Model model,@PathVariable("id")Long id){
		LOGGER.info("进入客户详情{}",id);
		ShopCustomer shopCustomer = customerService.selectByPrimaryKey(id);
		model.addAttribute("customer", shopCustomer);
        return "/admin/app/customer_details";
    }
	/**
	 * 新增和修改
	 * @return
	 */
	@RequestMapping(value = "/customer",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult customer(HttpServletRequest request,@RequestBody ShopCustomer customer){
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		customer.setShopId(user.getShopId());
		customer.setCreatorId(user.getId());
		customer.setCreatedTime(new Date());
		customer.setModifierId(user.getId());
		customer.setModifiedTime(new Date());
		ServiceResult serviceResult = customerService.insertOrUpdate(customer);
        return new AjaxResult(serviceResult);
    }
	@RequestMapping(value = "/customer-query",method = RequestMethod.POST)
	@ResponseBody
    public AjaxResult queryUser(HttpServletRequest request,@RequestBody CustomerQuery customerQuery){
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		customerQuery.setShopId(user.getShopId());
		customerQuery.setUser(user);
		//设置分页
		customerQuery.buildPage("");
		PageInfo<ShopCustomer> pageInfo = customerService.selectShopCustomer(customerQuery);
		return new AjaxResult(true,pageInfo);
	}
	
	/**
	 * 下单时的失去焦点事件
	 * @param request
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/customer-query/{mobile}",method = RequestMethod.GET)
	@ResponseBody
    public AjaxResult queryUserByLoginName(HttpServletRequest request,@PathVariable("mobile")Long mobile){
		User user = (User)request.getAttribute(CommonConstants.USER_INFO);
		ShopCustomer customer = customerService.selectByShopIdAndMobile(user.getShopId(), mobile);
		if(customer == null){
			return new AjaxResult(false);
		}else{
			return new AjaxResult(true,customer);
		}
	}
}
